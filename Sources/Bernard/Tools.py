"""
Various tools related to path management and third party applications (3DE, Nuke etc.)
"""




class BernardTools:
    
    @ classmethod
    def GetFileSeqenceInfos(cls, pathValue):#TODO: doc
        """Retrieve file sequence information contained in a Bernard path
        
        Example::
        
            from Bernard.Tools import BernardTools
            
            
            pathValue = "C:\\Project\\SeqA\\Shot01.[010:020 (11)].jpg"
            
            result = BernardTools.GetFileSeqenceInfos(pathValue)
            # >>> (10, 20, 3, 24, 38)
            
            if result is not None:
                start, end, padding, startIndex, endIndex = result
                
            
            pathValue = "C:\\Project\\SeqA\\Shot01_v03.jpg"
            
            result = BernardTools.GetFileSeqenceInfos(pathValue)
            # >>> None
            
        
        Args:
            pathValue(str):
            
        Returns: 
            (list or None): A list containing:
             - start (int)
             - end (int)
             - padding (int)
             - start file sequence info index (int)
             - end file sequence info index (int)
            or None if path sequence form not found
        """
        
        if "[" not in pathValue or "]" not in pathValue:
            return None, None, None, None, None
        
        reversTxt = pathValue[::-1]
        sequStartIndex = len(pathValue) - reversTxt.find("[") -1 
        sequEndIndex = len(pathValue) - reversTxt.find("]")
        
        sequenceInfo = pathValue[sequStartIndex+1 : sequEndIndex-1]
        
        startTxt = sequenceInfo.split(":")[0]
        endTxt = sequenceInfo.split(":")[-1].split(" ")[0]
    
        return int(startTxt), int(endTxt), len(startTxt), sequStartIndex, sequEndIndex
    
    
    @ classmethod
    def SequencePathToPaths(cls, pathValue):#TODO: doc
        """ 
        
        Example::
        
            from Bernard.Tools import BernardTools
            
            
            pathValue = "C:\\Project\\SeqA\\Shot01.[010:020 (11)].jpg"
            
            pathList = BernardTools.SequencePathToPaths(pathValue)
            # >>> [ "C:\\Project\\SeqA\\Shot01.010.jpg",
            #       "C:\\Project\\SeqA\\Shot01.011.jpg",
            #        ...]
            
            
            pathValue = "C:\\Project\\SeqA\\Shot01_v03.jpg"
            
            pathList = BernardTools.SequencePathToPaths(pathValue)
            # >>> [] 
                    
        Args:
            pathValue(str):
            
        Returns:
            (list):
        """
        
        start, end, padding, sequStartIndex, sequEndIndex = cls.GetFileSeqenceInfos(pathValue)
        
        if start is None:
            return [pathValue]
        
        else:
            startPathPart = pathValue[ : sequStartIndex]
            endPathPart = pathValue[sequEndIndex : ]
            
            paths = []
            for number in range(start, end+1):
                strNumber = str(number).zfill(padding)
                
                interpretedPath = startPathPart + strNumber + endPathPart
                paths.append(interpretedPath)
        
            return paths
 






