"""
Statics values to be used with the Bernard API
"""

class State:
    """
    Tasks in Bernard have states that are used by the state machine to calculate the workflow state

    A state is a description of the current status of one (1) task relative to its end goal.
    A state for a "normal" or "validaton" task  can only take 6 values:
    - Waiting (Under current conditions, the task cannot be started)
    - Ready (Under current conditions, the task can be started)
    - W.I.P (The task was started by a user, and he is active on this task)
    - Pause (The task was started by a user, but he is not currently active on this task)
    - Done (The task is considered completed by a user)
    - Aborted (The task is aborted.)

    Script task have a supplementary state:
    - Failed (when the executed operation return an error code)
    """
    waiting = 1
    ready   = 2
    pause   = 7
    wip     = 3
    done    = 4
    aborted = 5
    failed  = 6


class ClassName:
    """
    ClassName are used to identify Bernard's object type
    Theses names are the same as the internal name of the object's classes in Bernard source code
    """
    # Project Level
    root = "CNodeRoot"
    projectTemplate = "CNodeProjectTemplate"
    project = "CNodeProject"
    planning = "CNodePlanning"
    
    # Resources
    # inherit from CNodeRessource
    resource = "CNodeResource"
    user = "CNodeUser"
    computer = "CNodeComputer"

    # tasks
    # inherit from CNodeTask
    task = "CNodeTask"
    taskScript ="CNodeTaskScript"
    taskValidation = "CNodeTaskValidation"

    # Items
    projectItem = "CNodeProjectItem"
    item = "CNodeItem"

    # Attributes, classification, storage
    category = "CNodeCategory"
    file = "CNodeFile"
    department = "CNodeDepartment"
    property = "CNodeProperty"
    defaultProperty = "CNodeDefaultProperty"

    # Containers
    compound = "CNodeCompound"
    workflow = "CNodeWorkflow"
    folder = "CNodeFolder"
    lockedFolder = "CNodeLockedFolder"
    group = "CNodeGroup"
    exclusiveGroup = "CNodeExclusiveGroup"

    # Models
    projectModel = "CNodeProjectModel"
    projectItemModel = "CNodeProjectItemModel"
    itemModel = "CNodeItemModel"
    categoryModel = "CNodeCategoryModel"
    userModel = "CNodeUserModel"
    computerModel = "CNodeComputerModel"
    resourceModel = "CNodeResourceModel"



class PropertyType:
    """PropertyType is obviously used to identify Properties type
    PropertyType can be found using the Bernard GUI.
    |
    Howto : Select the very property, go to the "property view" window,
    under the attributes tab, you can read the 'Value Type'
    
    Warning :: In Bernard's GUI, the following types of properties do not have the same name as in the API
    (in order to allow users to better understand their usage)
     - PropertyType.item = 'List single choice'
     - PropertyType.id = 'List single choice'
     - PropertyType.ids = 'List multiples choice'
    """
    
    
    text        = 1  #(str)
    item        = 2  #(str or None)
    date        = 3  #(str or None): "%Y-%m-%d
    color       = 4  #(str)        : #346eeb
    dateTime    = 5  #(stror None ): "%Y-%m-%d, %H:%M:%S"
    
    
    float       = 10  #(float)
    int         = 11  #(int)
    state       = 12  #(Static.State)
    version     = 13  #(Int)
    duration    = 14  #(int) second

    instruction = 20  
    paths       = 21  
    
    boolean     = 30  #(bool)
    
    id          = 40  #(str or None)
    imageId     = 41  #(str, bytes or None)
    
    ids         = 50  #(list(str))
    files       = 53  #(list(str))
    items       = 54  #(list(str))
    assignment  = 55  
    timeLog     = 56  
    
    image       = 61  #(str, bytes or None)
    



class FileOperationType:
    """
    FileOperation indicate to the file management tools how the os
    file system has to transfer the files.
    """
    copy = 1
    move = 2

    


















