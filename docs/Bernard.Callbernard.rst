Bernard.CallBernard Class
=========================

.. autoclass:: Bernard.CallBernard
   :members:
   :undoc-members:
   :show-inheritance:

.. rubric:: Functions

.. autosummary::
  :toctree:
  