.. BernardAPI documentation master file, created by
   sphinx-quickstart on Wed Apr 14 16:21:06 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BernardAPI's documentation!
======================================

.. toctree::
   :maxdepth: 8
   :caption: Contents:

Class overview
--------------   

.. autosummary::
   :toctree: _autosummary
   :nosignatures:
   :recursive:

   Bernard.CallBernard
   Bernard.Tools
   Bernard.Static



Indices and tables

==================

   
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
